package tema2.tema2.project;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Casa implements Runnable {
	private BlockingQueue<Client> clienti;
	private AtomicInteger waitingPeriod;
	private int nrCasa;
	private Thread t1;
	private boolean running;
	private ViewTema2 frame;
	private int totalWaiting=0;
	

	public Casa(int nrCasa,ViewTema2 frame) {
		clienti = new ArrayBlockingQueue<Client>(15);
		this.nrCasa = nrCasa;
		Thread t1 = new Thread(this);
		t1.start();
		waitingPeriod=new AtomicInteger(0);
		this.frame=frame;
	}

	public BlockingQueue<Client> getClienti() {
		return clienti;
	}

	public void setClienti(BlockingQueue<Client> clienti) {
		this.clienti = clienti;
	}

	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public int getNrCasa() {
		return nrCasa;
	}

	public void setNrCasa(int nrCasa) {
		this.nrCasa = nrCasa;
	}

	public synchronized void addClient(Client newClient) throws InterruptedException {
	    //Thread.sleep(1000);
		
			clienti.put(newClient);  
		newClient.setWaitingTime(this.waitingPeriod.get()+newClient.getProcessingTime());
		frame.setTextArea(frame.getTextArea()+"\n"+newClient.toString()+" a intrat la casa "+this.getNrCasa());
		System.out.println(newClient.toString()+" a intrat la casa "+this.getNrCasa());
		if(this.nrCasa==1) {
			frame.setCoada1Text(frame.getCoada1Text()+"C"+newClient.getcID()+" ");
		}else if(this.nrCasa==2) {
			frame.setCoada2Text(frame.getCoada2Text()+"C"+newClient.getcID()+" ");
		}else if(this.nrCasa==3) {
			frame.setCoada3Text(frame.getCoada3Text()+"C"+newClient.getcID()+" ");
		}else if(this.nrCasa==4) {
			frame.setCoada4Text(frame.getCoada4Text()+"C"+newClient.getcID()+" ");
		}else if(this.nrCasa==5) {
			frame.setCoada5Text(frame.getCoada5Text()+"C"+newClient.getcID()+" ");
		}else if(this.nrCasa==6) {
			frame.setCoada6Text(frame.getCoada6Text()+"C"+newClient.getcID()+" ");
		}
	    this.waitingPeriod.addAndGet(newClient.getProcessingTime());
	    totalWaiting+=waitingPeriod.addAndGet(0);
	    
	}

	public Client scoateClient() throws InterruptedException {
		Client newC = clienti.element();
		Thread.sleep(newC.getProcessingTime()*1000);
		Client C = clienti.take();
		int x = C.getProcessingTime();
		this.waitingPeriod.set(this.waitingPeriod.get() - x);
		frame.setTextArea(frame.getTextArea()+"\n"+"Clientul " + C.getcID() + " a fost procesat la casa "+this.getNrCasa());
		System.out.println("Clientul " + C.getcID() + " a fost procesat la casa "+this.getNrCasa());
		if(this.nrCasa==1) {
			String text=frame.getCoada1Text();
			int a=text.indexOf(' ');
			frame.setCoada1Text(text.substring(a+1));
		}else if(this.nrCasa==2) {
			String text=frame.getCoada2Text();
			int a=text.indexOf(' ');
			frame.setCoada2Text(text.substring(a+1));
		}else if(this.nrCasa==3) {
			String text=frame.getCoada3Text();
			int a=text.indexOf(' ');
			frame.setCoada3Text(text.substring(a+1));
		}else if(this.nrCasa==4) {
			String text=frame.getCoada4Text();
			int a=text.indexOf(' ');
			frame.setCoada4Text(text.substring(a+1));
		}else if(this.nrCasa==5) {
			String text=frame.getCoada5Text();
			int a=text.indexOf(' ');
			frame.setCoada5Text(text.substring(a+1));
		}else if(this.nrCasa==6) {
			String text=frame.getCoada6Text();
			int a=text.indexOf(' ');
			frame.setCoada6Text(text.substring(a+1));
		}
      return C;
	}

	public int getNrClienti() {
		return clienti.size();
	}

	public void run() {
         running=true;
         
		while (running) {
			while (clienti.isEmpty() == false) {
				try {
					
					this.scoateClient();
					

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public BlockingQueue<Client> getClient() {
          return clienti;
          
	}
	
	public int getTotalWaiting() {
		return totalWaiting;
	}

}
