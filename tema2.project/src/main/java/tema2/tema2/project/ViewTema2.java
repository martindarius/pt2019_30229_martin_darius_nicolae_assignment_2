package tema2.tema2.project;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

public class ViewTema2 extends JFrame {
	
  private JLabel minarrivingTime=new JLabel("Minimum Arriving Time:");
  private JLabel maxarrivingTime=new JLabel("Maximum Arriving Time:");
  private JLabel minProcessingTime=new JLabel("Minimum Processing Time:");
  private JLabel maxProcessingTime=new JLabel("Maximum Processing Time:");
  private JLabel nrClients=new JLabel("Nr of clients:");
  private JLabel nrQueues=new JLabel("Nr. of queues:");
  private JLabel timeLimit=new JLabel("Time limit:");
  private JLabel LogEvents=new JLabel("Log of events:                                                                                        ");
  
  private JTextField nrClientsText=new JTextField(30);
  private JTextField minArrTimeText=new JTextField(30);
  private JTextField maxArrTimeText=new JTextField(30);
  private JTextField minTimeText=new JTextField(30);
  private JTextField maxTimeText=new JTextField(30);
  private JTextField nrQueuesText=new JTextField(30);
  private JTextField timeLimitText=new JTextField(30);
  private JButton submitt=new JButton("Submitt");
  private JTextArea text=new JTextArea(300,300);
  
  private JLabel coada1=new JLabel("Q1:");
  private JTextField coada1text=new JTextField(200);
  private JLabel coada2=new JLabel("Q2:");
  private JTextField coada2text=new JTextField(100);
  private JLabel coada3=new JLabel("Q3:");
  private JTextField coada3text=new JTextField(100);
  private JLabel coada4=new JLabel("Q4:");
  private JTextField coada4text=new JTextField(100);
  private JLabel coada5=new JLabel("Q5:");
  private JTextField coada5text=new JTextField(100);
  private JLabel coada6=new JLabel("Q6:");
  private JTextField coada6text=new JTextField(100);
  
  
  
  public ViewTema2() {
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1900, 500);
		this.setTitle("Simulare cozi magazin:");
		JPanel c  = new JPanel();
		JPanel c1 = new JPanel();
		JPanel c2 = new JPanel();
		JPanel c3 = new JPanel();
 		
		c1.add(minarrivingTime);
		c1.add(minArrTimeText);
		c1.add(maxarrivingTime);
		c1.add(maxArrTimeText);
		c1.add(minProcessingTime);
		c1.add(minTimeText);
		c1.add(maxProcessingTime);
		c1.add(maxTimeText);
		c1.add(nrQueues);
		c1.add(nrQueuesText);
		c1.add(timeLimit);
		c1.add(timeLimitText);
		c1.add(nrClients);
		c1.add(nrClientsText);
		c1.add(submitt);
		
		c1.setLayout(new GridLayout(8,2));
		
		c.add(c1);
		
		c2.add(LogEvents);
		text.setPreferredSize(new Dimension(300,300));
		text.setEditable(false);
		JScrollPane scroll=new JScrollPane(text);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setPreferredSize(new Dimension(500,500));
		c2.add(scroll);
		
		c2.setLayout(new BoxLayout(c2,BoxLayout.Y_AXIS));
		
		
		c3.add(coada1);
		c3.add(coada1text);
		c3.add(coada2);
		c3.add(coada2text);
		c3.add(coada3);
		c3.add(coada3text);
		c3.add(coada4);
		c3.add(coada4text);
		c3.add(coada5);
		c3.add(coada5text);
		c3.add(coada6);
		c3.add(coada6text);
		
		coada1text.setEditable(false);
		coada2text.setEditable(false);
		coada3text.setEditable(false);
		coada4text.setEditable(false);
		coada5text.setEditable(false);
		coada6text.setEditable(false);
		c3.setLayout(new BoxLayout(c3,BoxLayout.PAGE_AXIS));
		
		c.add(c3);
		
		c.add(c2);
		
        c.setLayout(new BoxLayout(c,BoxLayout.X_AXIS));
		this.setContentPane(c);
		this.setVisible(true);
  }
  
  
  public void butonSubmitt(ActionListener a) {
	  submitt.addActionListener(a);
	}
  
  public String getMinArrTime() {
	  return minArrTimeText.getText();
  }
  
  public String getMaxArrTime() {
	  return maxArrTimeText.getText();
  }
  public String getMinProcTime() {
	  return minTimeText.getText();
  }
  public String getMaxProcTime() {
	  return maxTimeText.getText();
  }
  
  public String getNrQ() {
	  return nrQueuesText.getText();
  }
  
  public String getTimeLimit() {
	  return timeLimitText.getText();
  }
  
  public String getNrClients() {
	  return nrClientsText.getText();
  }
  
  public void setTextArea(String text) {
	  this.text.setText(text);
  }
  public String getTextArea() {
	  return text.getText();
  }
  
  public void showException(String mesaj) {
		JOptionPane.showMessageDialog(this,mesaj);
	}
  
  public void showStatistics(String mesaj) {
	  JOptionPane.showMessageDialog(this,mesaj);
  }
  
  public String getCoada1Text() {
	  return coada1text.getText();
  }
  public void setCoada1Text(String text) {
	  coada1text.setText(text);
  }
  public String getCoada2Text() {
	  return coada2text.getText();
  }
  public void setCoada2Text(String text) {
	  coada2text.setText(text);
  }
  public String getCoada3Text() {
	  return coada3text.getText();
  }
  public void setCoada3Text(String text) {
	  coada3text.setText(text);
  }
  public String getCoada4Text() {
	  return coada1text.getText();
  }
  public void setCoada4Text(String text) {
	  coada1text.setText(text);
  }
  public String getCoada5Text() {
	  return coada1text.getText();
  }
  public void setCoada5Text(String text) {
	  coada1text.setText(text);
  }
  public String getCoada6Text() {
	  return coada1text.getText();
  }
  public void setCoada6Text(String text) {
	  coada1text.setText(text);
  }
  
}
