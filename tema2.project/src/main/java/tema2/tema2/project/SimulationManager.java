package tema2.tema2.project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.swing.text.View;

public class SimulationManager implements Runnable {
	public int timeLimit=10;
	public int maxProcessingTime;
	public int minProcessingTime;
	public int numberOfClients;
	public int maxArrTime;
	public int minArrTime;
	public int numarCase;
	public Thread thread;
	public int porneste = 0;
	private ArrayList<Client> totiClientii = new ArrayList<Client>();

	private Scheduler scheduler;

	private ViewTema2 frame;

	private List<Client> generatedClient;

	class butonSubmitt implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			String timeLimitText = "", maxProcTimeText = "", minProcTimeText = "", maxArrTimeText = "",
					minArrTimeText = "", nrCaseText = "",nrClientsText="";
            nrClientsText=frame.getNrClients(); 
			timeLimitText = frame.getTimeLimit();
			maxProcTimeText = frame.getMaxProcTime();
			minProcTimeText = frame.getMinProcTime();
			maxArrTimeText = frame.getMaxArrTime();
			minArrTimeText = frame.getMinArrTime();
			nrCaseText = frame.getNrQ();
			System.out.println(timeLimitText + " " + maxProcTimeText + " " + minProcTimeText + " " + maxArrTimeText
					+ " " + minArrTimeText + " " + nrCaseText);
			try {
                numberOfClients=Integer.parseInt(nrClientsText); 
				timeLimit = Integer.parseInt(timeLimitText);
				maxProcessingTime = Integer.parseInt(maxProcTimeText);
				minProcessingTime = Integer.parseInt(minProcTimeText);
				maxArrTime = Integer.parseInt(maxArrTimeText);
				minArrTime = Integer.parseInt(minArrTimeText);
				numarCase = Integer.parseInt(nrCaseText);

			} catch (NumberFormatException e) {
				frame.showException("Imput invalid!");
			}
			porneste = 1;
			generateNRandomClient();
			scheduler = new Scheduler(numarCase,frame);
			System.out.println(porneste);
		}

	}

	public SimulationManager(ViewTema2 view) {
		this.frame = view;
		frame.butonSubmitt(new butonSubmitt());
		

	}

	private void generateNRandomClient() {
		for (int i = 0; i < numberOfClients; i++) {
			Random r = new Random();
			int procTime = r.nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime;
			int arrTime = r.nextInt(maxArrTime - minArrTime) + minArrTime;
			// System.out.println(procTime+" "+arrTime);
			Client C = new Client(i + 1, arrTime, procTime,0);
			totiClientii.add(C);

		}
		Collections.sort(totiClientii);
		for (Client i : totiClientii) {
			System.out.println(i.getcID() + " " + i.getArrivalTime() + " " + i.getProcessingTime());
		}
	}

	public void run() {

		int currentTime = 0;
        int time=0;
        int peak=0;
		while (currentTime < timeLimit) {


			if (porneste == 1) {
				for (Client i : totiClientii) {
					if (i.getArrivalTime() == currentTime) {
						scheduler.distribuie(i);
						// totiClientii.remove(i);
					}

				}
				
				if(scheduler.getMaxClienti()>peak) {
					
					peak=scheduler.getMaxClienti();
					time=currentTime;
				}
				// opresti run din case
				currentTime++;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else 
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		int suma=0;
		for(Casa i:scheduler.getCasa()) {
			int x=i.getTotalWaiting();
			suma+=x;
		}
		double medie=(double)suma/totiClientii.size();
		frame.showStatistics("Timpul total de asteptare a fost: "+suma+"\n Timpul mediu de asteptare a fost: "+medie+"\n Peak time: "+time);
		
		

	}

	public static void main(String[] args) {
		ViewTema2 a = new ViewTema2();
		SimulationManager gen = new SimulationManager(a);
		Thread t=new Thread(gen);
		 t.start();
	}

}
