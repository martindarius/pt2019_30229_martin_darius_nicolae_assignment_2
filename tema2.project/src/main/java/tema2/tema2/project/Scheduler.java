package tema2.tema2.project;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
 private List<Casa> Case = new ArrayList<Casa>();
 private int maxNrCase;
 private int maxClientPerCasa;
 private ViewTema2 frame;
 
 public Scheduler(int maxNrCase,ViewTema2 frame) {
	 this.frame=frame;
	 for(int i=0;i<maxNrCase;i++) {
		 Casa c=new Casa(i+1,frame);
		 Case.add(c);
	 }
 }
 

 
 public  void distribuie(Client client){
	int x=coadaMinima();
	
	try {
		//Thread.sleep(1000);
		Case.get(x-1).addClient(client);
	//	System.out.println(Case.get(x-1).getNrCasa());
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
 }
 public List<Casa> getCasa(){
	return Case;
 }
 public int coadaMinima() {
	 int index = 1;
	 int x=Integer.MAX_VALUE;
	 for(Casa i:Case) {
		 if (i.getNrClienti()<x) {
			 x=i.getNrClienti();
			 index=i.getNrCasa();
			 
		 }
	 }
	 System.out.println("nr clienti din casa: " + index + "  " + x);
	 return index;
 }
 
 public int getMaxClienti() {
	 int x=0;
	 for(Casa i:this.Case) {
		 x+=i.getNrClienti();
	 }
	 //System.out.println(x);
	 return x;
 }
}
