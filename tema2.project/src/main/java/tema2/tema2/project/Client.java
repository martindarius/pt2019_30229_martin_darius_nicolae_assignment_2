package tema2.tema2.project;

public class Client implements Comparable<Client> {
   private int cID;
   private int arrivalTime;
   private int processingTime;
   private int waitingTime=0;


   

public int getWaitingTime() {
	return waitingTime;
}
public void setWaitingTime(int waitingTime) {
	this.waitingTime = waitingTime;
}
public Client(int cId,int arrivalTime, int processingTime,int waitingTime) {
	super();
	this.waitingTime=waitingTime;
	this.cID=cId;
	this.arrivalTime = arrivalTime;
	this.processingTime = processingTime;
	
}
public int getArrivalTime() {
	return arrivalTime;
}
public void setArrivalTime(int arrivalTime) {
	this.arrivalTime = arrivalTime;
}
public int getProcessingTime() {
	return processingTime;
}
public void setProcessingTime(int processingTime) {
	this.processingTime = processingTime;
}
public int getcID() {
	return cID;
}
public void setcID(int cID) {
	this.cID = cID;
}
@Override
public String toString() {
	return "Clientul nr." + cID + " cu arrivalTime=" + arrivalTime + " si  processingTime=" + processingTime;
}

public int compareTo(Client a) {
	if(((Client)a).getArrivalTime()>this.getArrivalTime())
		return -1;
	else if(((Client)a).getArrivalTime()<this.getArrivalTime())
		return 1; 
	else 
	    return 0;
}
   

}
